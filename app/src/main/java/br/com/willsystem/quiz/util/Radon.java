package br.com.willsystem.quiz.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by will on 20/09/16.
 */
public class Radon {



    public static Integer nextInt(){
        Random gerador = new Random();
        Integer num = 0;
        do {
            num = gerador.nextInt(51);
        }while (num == 0);
        return num;
    }

    public static List<Integer> nextIntEspecific(Integer quantidade){
        List<Integer> numquestoes = new ArrayList<>();
        do {


            Boolean repetido = false;
            Integer num = 1;

                num = nextInt();
                for (Integer numero : numquestoes
                        ) {
                    if (numero == num) {
                        repetido = true;
                    }
                }
                if (!repetido) {
                    numquestoes.add(num);
                    System.out.println(num);
                }else{
                    System.err.println(num);
                }


        }while(numquestoes.size() < quantidade);
        return numquestoes;

    }
}
