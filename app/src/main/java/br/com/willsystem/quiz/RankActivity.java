package br.com.willsystem.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.view.View.OnClickListener;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.quiz.api.VolleyApplication;
import br.com.willsystem.quiz.model.Partida;
import br.com.willsystem.quiz.repository.RepositoryPartida;

public class RankActivity extends AppCompatActivity implements OnClickListener {

    private ListView listaDePartidas;
    private List<Partida> partidas = new ArrayList<>() ;
    ArrayAdapter<Partida> adapter ;
    private Button btn,btnrankonly;
    private RepositoryPartida repositorio = new RepositoryPartida(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);

        listaDePartidas = (ListView) findViewById(R.id.listaRank);
        // listatext = new RepositoryPergunta(this).find();
        btn = (Button) findViewById(R.id.btnInicio2) ;
        btnrankonly = (Button) findViewById(R.id.btnRankOnline);
        partidas = repositorio.partidas();

        btnrankonly.setOnClickListener(this);
        btn.setOnClickListener(this);

        adapter = new ArrayAdapter<Partida>
                (this,android.R.layout.simple_expandable_list_item_1,partidas);
        listaDePartidas.setAdapter(adapter);


    }



    @Override
    public void onClick(View view) {
        if (view.equals(btn)){
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
            finish();
        }else if(view.equals(btnrankonly)){
            Intent intent = new Intent(this,RankOnlineActivity.class);
            startActivity(intent);
            finish();
        }
    }
}

