package br.com.willsystem.quiz.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.quiz.banco.DBUtil;
import br.com.willsystem.quiz.model.Partida;

/**
 * Created by will on 20/09/16.
 */
public class RepositoryPartida {

    private static DBUtil dbHelper;


    public RepositoryPartida(Context ctx){
        dbHelper = new DBUtil(ctx, "game-android-quiz", 1);
    }

    public void salvar(Partida partida){
        ContentValues valores = new ContentValues();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        valores.put("jogador",partida.getJogador());
        valores.put("pontos",partida.getPontos());
        long num = db.insert("partida",null,valores);
        Log.i("salvarPartida","PartidaSalva"+num);

    }

    public List<Partida> partidas(){
        SQLiteDatabase db ;
        try {

            db = dbHelper.getReadableDatabase();
            List<Partida> lista = new ArrayList<>();
            String sql ="select * from partida ORDER BY pontos DESC LIMIT 5" ;
            Cursor cursor = db.rawQuery(sql,null);
            Partida partida;
            if(cursor.moveToFirst()){
                do{
                    partida = new Partida();
                    partida.setId(cursor.getInt(0));
                    partida.setJogador(cursor.getString(1));
                    partida.setPontos(cursor.getString(2));
                    lista.add(partida);
                }while(cursor.moveToNext());

            }


            Log.d("STATUS","FOI");
            return lista;

        }catch (Exception e ){
            return new ArrayList<>();
        }
    }

    public static List<Partida> popular() {
        List<Partida> partidas = new ArrayList<>();
        partidas.add(new Partida("WIlton","200"));
        partidas.add(new Partida("WIlton","190"));
        partidas.add(new Partida("WIlton","180"));
        partidas.add(new Partida("WIlton","170"));
        partidas.add(new Partida("WIlton","160"));
        partidas.add(new Partida("WIlton","150"));
        partidas.add(new Partida("WIlton","140"));
        return partidas;

    }
}
