package br.com.willsystem.quiz;

import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.quiz.model.Perguntas;

/**
 * Created by will on 16/08/16.
 */
public class PerguntaServiceCopia {


    public static List<Perguntas> perguntas(){


        Perguntas pergunta1 = new Perguntas("Qual desses números é um numero inteiro ?","1","2.5","7/2","4.12230","1");
        Perguntas pergunta2 = new Perguntas("QUal número é maior que -1 ?","0","-30","-39","-1.8","1");
        Perguntas pergunta3 = new Perguntas("Qual o resultado da expressão 5*7+2-1","35","48","40","36","4");
        Perguntas pergunta4 = new Perguntas("Qual das opções representa a inversão de um resultado lógico ?","AND","OUR"," NOT","NOT RES","3");
        Perguntas pergunta5 = new Perguntas("QUal a opção que o resultado lógico será verdadeiro, somente se todas resposta for verdadeiro ?",
                "NOT","AND","OUR","IN","2");
        Perguntas pergunta6 = new Perguntas("Um carro viaja 100km em 1 hora e outro percorreu 50 kilômetros em 30 minutos, se os dois sairem" +
                "ao mesmo tempo, qual a resposta correta ? ","Carro 1 chega primeiro"," Carro 2 chega primeiro","Ocorre empate","Nenhum chega",
                "3");
        Perguntas pergunta7 = new Perguntas("Uma pessoa escorrega e cai sentado no chão, qual o nome do filme ? ","Dia da independencia",
                "tropa de elite"," cidade de Deus","a cruzada","4");
        Perguntas pergunta8 = new Perguntas("Qual número é menor ?","0.7878","0.66565","0.01","0.010001","3");
        Perguntas pergunta9 = new Perguntas("Observando detalhes da interface gráfica, quem é o criador desse aplicativo ?",
                "Wilton","Fernando","Carla","Sania","1");
        Perguntas pergunta10 = new Perguntas("QUal a probabilidade de jogar uma moeda para cima e o resultado for cara ? ","20%","40%","80%",
                "50%","4");


        List<Perguntas> perguntas = new ArrayList<>();
        perguntas.add(pergunta1);
        perguntas.add(pergunta2);
        perguntas.add(pergunta3);
        perguntas.add(pergunta4);
        perguntas.add(pergunta5);
        perguntas.add(pergunta6);
        perguntas.add(pergunta7);
        perguntas.add(pergunta8);
        perguntas.add(pergunta9);
        perguntas.add(pergunta10);
        return perguntas;

    }



}
