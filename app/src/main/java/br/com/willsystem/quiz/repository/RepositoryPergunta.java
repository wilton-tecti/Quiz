package br.com.willsystem.quiz.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.quiz.PerguntaService;
import br.com.willsystem.quiz.model.Perguntas;
import br.com.willsystem.quiz.banco.DBUtil;

/**
 * Created by will on 18/08/16.
 */
public class RepositoryPergunta {

    private SQLiteDatabase db;
    private DBUtil dbHelper;



    public RepositoryPergunta (Context ctx){
        dbHelper = new DBUtil(ctx, "game-android-quiz", 1);
    }



    public long inserir(){
        ContentValues valores = new ContentValues();
        db = dbHelper.getReadableDatabase();
        Log.d("NOME BANCO",dbHelper.getDatabaseName());
        List<Perguntas> perguntas = PerguntaService.perguntas();
        long num = 0;
        for (int i= 0 ; i< perguntas.size(); i++){
            valores.put("questao",perguntas.get(i).getQuestao());
            valores.put("res1",perguntas.get(i).getResposta1());
            valores.put("res2",perguntas.get(i).getResposta2());
            valores.put("res3",perguntas.get(i).getResposta3());
            valores.put("res4",perguntas.get(i).getResposta4());
            valores.put("rescerta",perguntas.get(i).getRespostacerta());
           num = db.insert("pergunta",null,valores);
        }
        db.close();
        System.out.println("ADd");
        return num;
    }
    public void deletar(){
        db = dbHelper.getReadableDatabase();
        db.execSQL("");
    }

    public List<Perguntas> find(){
        try {

            db = dbHelper.getReadableDatabase();
            List<Perguntas> lista = new ArrayList<>();
            String sql ="select * from pergunta " ;
            Cursor cursor = db.rawQuery(sql,null);
            Perguntas perguntas;
            while(cursor.moveToNext()) {
                perguntas = new Perguntas();
                if (cursor != null && cursor.moveToFirst()) {
                    perguntas.setId(cursor.getInt(0));
                    perguntas.setQuestao(cursor.getString(1));
                    perguntas.setResposta1(cursor.getString(2));
                    perguntas.setResposta2(cursor.getString(3));
                    perguntas.setResposta3(cursor.getString(4));
                    perguntas.setResposta4(cursor.getString(5));
                    perguntas.setRespostacerta(cursor.getString(6));
                }
                lista.add(perguntas);
            }
            Log.d("STATUS","FOI");
            return lista;

        }catch (Exception e ){
            return new ArrayList<>();
        }
    }

    public Perguntas find(Integer codigo){
        try{
        db = dbHelper.getReadableDatabase();
        String sql ="select * from pergunta where _id = "+codigo ;
        Cursor cursor = db.rawQuery(sql,null);
        Perguntas perguntas = new Perguntas();
        if (cursor != null && cursor.moveToFirst()) {
            perguntas.setId(cursor.getInt(0));
            perguntas.setQuestao(cursor.getString(1));
            perguntas.setResposta1(cursor.getString(2));
            perguntas.setResposta2(cursor.getString(3));
            perguntas.setResposta3(cursor.getString(4));
            perguntas.setResposta4(cursor.getString(5));
            perguntas.setRespostacerta(cursor.getString(6));
        }else{perguntas = null;}
            Log.d("STATUS","FOI");
        return perguntas;
        }catch (Exception exp){
            Log.d("STATUS",exp.getMessage());
            return new Perguntas();
        }


    }


}
