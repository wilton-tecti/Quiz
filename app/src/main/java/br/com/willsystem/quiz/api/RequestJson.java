package br.com.willsystem.quiz.api;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.quiz.model.Partida;

/**
 * Created by 2898807 on 14/10/2016.
 */
public class RequestJson {
    private static final String SERVIDOR = "http://52.67.128.201";
    private static final String PORTA = "80";
    private static final String API = "/api/";
    private static final String URL = SERVIDOR+API;
    private List<Partida> partidas ;

    public void salvarPartidaRankOnline(Partida partida){
        String url = URL+"partida?jogador="+partida.getJogador()+"&pontos="+partida.getPontos();
            Log.e("URL",url);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("json", error.getMessage());
            }
        });
        VolleyApplication.getInstance().getRequestQueue().add(request);
    }

    public List<Partida> listarRank(){
        String url = URL+"/partidas";
         partidas = new ArrayList<>();


        Log.e("url", url);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        convertJsonParaPartida(response, partidas);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("json", error.getMessage());
            }
        });



        VolleyApplication.getInstance().getRequestQueue().add(request);
        return partidas;
    }

    private List<Partida> convertJsonParaPartida(JSONArray response, List<Partida> partidas){

        try {
            Log.e("json", response.getString(0));
            Gson gson = new Gson();
            Type type = new TypeToken<Partida>(){}.getType();
            for (int i =0 ;i < response.length(); i++) {
                Partida partida = gson.fromJson(response.getString(i), Partida.class);
                partidas.add(partida);

            }

        } catch (JsonSyntaxException j){
            Log.e("jsonSyntaxExce",j.getMessage());
            j.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return partidas;

    }
}
