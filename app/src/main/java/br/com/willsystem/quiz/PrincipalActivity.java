package br.com.willsystem.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener ;
import android.widget.Button;
import android.widget.EditText;

import br.com.willsystem.quiz.repository.RepositoryPartida;
import br.com.willsystem.quiz.repository.RepositoryPergunta;

public class PrincipalActivity extends AppCompatActivity  implements OnClickListener {


    private Button btn, btnrank, btnadmin;
    private EditText nome;
    private AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        System.out.println("Instanciou PrincipalActivity");
        btn = (Button) findViewById(R.id.btnPerguntas);
        btnrank = (Button) findViewById(R.id.btnRankprinc);
        nome = (EditText) findViewById(R.id.editNome);
        btnadmin = (Button) findViewById(R.id.btnadmin);

        Intent intent = getIntent();
        try {
            nome.setText(intent.getStringExtra("nome").toString());
        }catch (Exception e){
            //nome.setText("");
        }
        if (new RepositoryPergunta(this).find(0) == null){
            new RepositoryPergunta(this).inserir();
        }

        btn.setOnClickListener(this);
        btnrank.setOnClickListener(this);
        btnadmin.setOnClickListener(this);




    }

    @Override
    public void onClick(View view) {
        if (view.equals(btn)){
                if (TextUtils.isEmpty(nome.getText())){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Atenção");
                    builder.setMessage("O campo nome não pode estar em branco" );
                    alerta = builder.create();
                    alerta.show();

                }else if(nome.getText().equals("www")){
                    Intent intent = new Intent(this,AdminActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(this,MainActivity.class);
                    intent.putExtra("nome",String.valueOf(nome.getText()));
                    startActivity(intent);
                    finish();

                }

        }else if(view.equals(btnrank)){
            Intent intent = new Intent(this,RankActivity.class);
            startActivity(intent);
            finish();

        }else if(view.equals(btnadmin)){
            Intent intent = new Intent(this,AdminActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
