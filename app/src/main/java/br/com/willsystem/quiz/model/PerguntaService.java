package br.com.willsystem.quiz.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 16/08/16.
 */
public class PerguntaService {


    public static List<Perguntas> perguntas() {


        Perguntas pergunta1 = new Perguntas("Acerca de Pilha é correto afirmar",
                "Lista linear em que inserções e remoções são feitas em ambos os extremos",
                "Lista linear em que as inserções, remoções e acesso são feitos num mesmo extremo denominado topo",
                "Lista linear em que as inserções são feitas num extremo, denominado final, e as remoções são feitas no extremo oposto",
                "Lista linear em que as inserções, remoções e acesso são feitos num mesmo extremo denominado início",
                "2");
        Perguntas pergunta2 = new Perguntas("Lista do tipo LIFO (Last In, First Out) é uma característica da estrutura de dados denominada.",
                "Grafo",
                "Fila",
                "Árvore",
                "Pilha",
                "4");
        Perguntas pergunta3 = new Perguntas("Dada a sequência (13, 22, 25, 26, 56, 48, 12, 10) em forma de Fila, pegue cada item de forma sequencial e coloque em uma pilha. Assinale a assertiva correta",
                "Início - 10, 22, 25, 26, 56 ,48, 12, 13- final.",
                "início - 13, 22, 25, 26, 56 ,48, 12, 10 - final.",
                "início - 10, 12, 13, 22, 25, 26, 48, 56- final",
                "início - 10, 12, 48, 56, 26, 25, 22, 13 - final",
                "4");
        Perguntas pergunta4 = new Perguntas("Acerca de Pilhas é correto afirmar que as ações de adicionar e retirar um item correspondem respectivamente às operações.",
                "Pull - Push",
                "Pop - Pull",
                "Push - Pop",
                "Pop - Push",
                "3");
        Perguntas pergunta5 = new Perguntas("Em termos de programação, tratando-se de estrutura de armazenamento, uma pilha é composta de:",
                "Um vetor que armazena os itens da pilha e um índice que indica a posição do item no topo da pilha.",
                "Um vetor que armazena os itens da pilha e um índice que indica a posição do item no início da pilha",
                "Push (Adiciona um novo item no final da fila) - Pop (Retira o último item da fila).",
                "Um vetor que armazena os itens da pilha, um índice que indica a posição do item no início da pilha e outro índice que indica a posição do item no final da pilha.",
                "1");
        Perguntas pergunta6 = new Perguntas("Assinale a alternativa correta.",
                "Fila é um tipo de lista linear em que as inserções são feitas num extremo, chamado começo, e as remoções são feitas no outro, chamado final.",
                "Fila é um tipo de lista linear em que as inserções são feitas num extremo, chamado final, e as remoções são feitas no outro, chamado começo.",
                "Lista do tipo LIFO( Last In, First Out) é uma característica da estrutura de dados denominada Fila.",
                "As entradas e saídas de uma lista do tipo LIFO acontecem na mesma ordem.",
                "2");
        Perguntas pergunta7 = new Perguntas("Em termos de programação, uma fila é composta de:",
                "Um vetor, que armazena seus itens em sequência e dois índices, sendo um para indicar a posição do vetor em que se encontra o primeiro item da fila e o outro para indicar a posição do vetor em que o próximo item inserido na fila será armazenado.",
                "Um vetor, que armazena seus itens em sequência e um índice, para indicar a posição do vetor em que se encontra o primeiro item da fila.",
                "Um vetor, que armazena seus itens em sequência e um índice, para indicar a posição do vetor em que o próximo item inserido na fila será armazenado.",
                "Um vetor, que armazena seus itens de forma aleatória e dois índices, sendo uma para indicar o início e o outro para indicar o final do vetor.",
                "1");
        Perguntas pergunta8 = new Perguntas("Considerando que a sequência ( topo - 10, 3, 5, 45, 25, 12 - início) é uma pilha com origem de uma fila onde seus itens foram colocados de forma inversa. Assinale a alternativa correspondente à sequência original.",
                "início - 3, 5, 10, 12, 25, 45 - fim",
                "início - 45, 25, 12, 10, 5, 3 - fim",
                "início - 10, 3, 5, 45, 25, 12 - fim",
                "fim - 10, 3, 5, 45, 25, 12 - início",
                "3");
        Perguntas pergunta9 = new Perguntas("Acerca de Alocação dinâmica de memória. Assinale a alternativa correta",
                "dispose(p), aloca um novo bloco de memória apontado pelo ponteiro p.",
                "new(p), desaloca o bloco de memória apontado pelo ponteiro p",
                "new(p), só funciona corretamente quando p armazena o endereço de uma área que foi previamente alocada com o dispose(p).",
                "dispose(p), desaloca o bloco de memória apontado pelo ponteiro p",
                "4");
        Perguntas pergunta10 = new Perguntas("Em relação aos riscos que se tem pelo fato de um vetor possuir um tamanho fixo. Assinale a alternativa incorreta",
                "Superdimensionar o vetor, ou seja, alocar espaço para carregar qualquer registro tornando o lento e pesado.",
                "Subdimensionar o vetor, ou seja, alocar menos espaço do que aquele realmente necessário",
                "Superdimensionar o vetor, ou seja, alocar mais espaço do que aquele realmente necessário.",
                "Subdimensionar o vetor, neste caso, em algum momento durante a execução do programa, o espaço alocado será insuficiente para guardar todos os itens, portanto o programa vai terminar com um erro fatal.",
                "1");
        Perguntas pergunta11 = new Perguntas("acerca de listas ordenadas. Marque a alternativa incorreta.",
                "Como a existência de itens repetidos não viola as propriedades de lista ordenada, a operação insere(x, L), sempre insere um novo item x em L, mesmo que já exista em L.",
                "No caso da operação remove(x, L), se houver mais que uma ocorrência do item x, apenas a primeira delas será removida.",
                "No caso da operação remove(x, L), se houver mais que uma ocorrência do item, todas serão removidas.",
                "A operação primeiro(L) acessa o primeiro item de L, enquanto a operação resto(L) acessa o resto de L, pulando o primeiro item.",
                "3");
        Perguntas pergunta12 = new Perguntas("Acerca de remoção em Lista Ordenada. Marque a alternativa correta",
                "Ao excluir um item x de uma lista L onde x é menor que o primeiro item, concluímos que é necessário percorrer a lista de forma inversa até encontrar o item x. ",
                "Ao excluir um item x de uma lista L onde x é menor que o primeiro item, concluímos que ele não está em L, portanto não há nada a ser feito.",
                "Ao excluir um item x de uma lista L onde x é maior que 1 em relação ao primeiro item, concluímos que ele não está em L.",
                "Ao excluir um item x de uma lista L onde x é menor que 1 em relação ao último item, concluímos que ele não está em L.",
                "2");
        Perguntas pergunta13 = new Perguntas("Sobre o uso de sentinelas assinale a alternativa correta.",
                "Sentinelas são blocos especiais mantidos nos extremos de uma lista encadeada com a finalidade de facilitar a implementação das operações efetuadas com a lista.",
                "Sentinelas são blocos especiais que podem ser inseridos no início, meio e fim de uma lista encadeada.",
                "para criar uma lista ordenada com sentinela no início, usamos o mesmo tipo lstord já definido para lista ordenada comum, alterando o endereço guardado o ponteiro quando necessário. ",
                "O bloco sentinela pode permanecer em qualquer lugar da lista porém o endereço guardado no ponteiro inicial nunca será é alterado.",
                "1");
        Perguntas pergunta14 = new Perguntas("Acerca de estrutura de dados do tipo árvore (A) é incorreto afirmar que:",
                "A raiz r é o único nó de A que não tem pai.",
                "Por definição, árvores são estruturas recursivas, ou seja removendo a raiz de uma árvore, obtemos uma coleção de árvores",
                "O nó r é pai dos nós r1, r2, r3, . . . rn.",
                "Uma árvore é uma estrutura linear composta por n nós. Se n = 0 então não existe uma árvore.",
                "4");
        Perguntas pergunta15 = new Perguntas("O número de filhos de um nó de uma árvore denomina-se:",
                "herdeiros",
                "nível",
                "grau",
                "classe",
                "3");
        Perguntas pergunta16 = new Perguntas("Acerca de árvores uma folha é um nó de grau:",
                "1",
                "0",
                "3",
                "2",
                "2");


        Perguntas pergunta17 = new Perguntas("Um percurso é uma forma sistemática de visitar cada nó de uma árvore exatamente uma vez. Podemos percorrer uma árvore de duas formas são elas:",
                "Profundidade e Largura",
                "Sequencial e Profundidade",
                "Largura e Sequencial",
                "Profundidade e Altura",
                "");
        Perguntas pergunta18 = new Perguntas("São tipo básicos de percursos em profundidade exceto:",
                "aleatório",
                "Em-ordem",
                "Pré-ordem",
                "Pós-ordem",
                "");
        Perguntas pergunta19 = new Perguntas("Assinale a alternativa incorreta Seja A uma árvore binária cuja raiz armazena um item r. Dizemos que A é uma árvore de busca binária, ou ordenada, se e somente se:",
                "todo item armazenado na subárvore esquerda de A é menor ou igual a r.",
                "Todo item armazenado na subárvore esquerda de A é maior que r.",
                "Todo item armazenado na subárvore direita de A é maior que r.",
                "Cada subárvore de A também é uma árvore de busca binária.",
                "");
        Perguntas pergunta20 = new Perguntas("I - A quantidade de memória destinada ao armazenamento de dados e determinada no momento da compilação e, durante a execução do programa, essa quantidade permanece constante. II - A quantidade de memória destinada ao armazenamento de dados varia enquanto o programa é executado. As afirmações acima respectivamente refere-se à:",
                "Alocação sequencial - Alocação dinâmica",
                "Alocação dinâmica - Alocação estática",
                "Alocação estática - Alocação dinâmica",
                "Alocação dinâmica- Alocação sequencial",
                "");
        Perguntas pergunta21 = new Perguntas("Acerca de estruturas de informação, assinale a opção correta.",
                "Um nó com grau maior que zero indica que o nó possui, pelo menos, um filho.",
                "a ordem da execução das operações para percorrer as subárvores da esquerda e da direita independe do tipo de caminhamento, seja ele prefixado, central ou pós-fixado.",
                "A quantidade de filhos de um nó denomina-se nível.",
                "Os nós ficam nas extremidades das árvores.",
                "");
        Perguntas pergunta22 = new Perguntas("As siglas e significados de LIFO (Last In) e FIFO First In, First Out referem-se, respectivamente, às seguintes estrutura de dados.",
                "Fila e Árvore",
                "Fila Pilha",
                "Árvore e Pilha",
                "Pilha e Fila",
                "");
        Perguntas pergunta23 = new Perguntas("No que se refere a estrutura de dados é INCORRETO afirmar:",
                "Pela definição de fila, se os elementos são inseridos por um extremo da lista linear, eles só podem ser removidos pelo outro.",
                "Numa fila dupla, os elementos podem ser inseridos e removidos de qualquer um dos extremos da fila.",
                "Em qualquer situação é possível usar uma única fila dupla para representar duas filas simples.",
                "A implementação de uma fila dupla normalmente é mais eficiente com uma lista duplamente encadeada que com uma encadeada simples.",
                "");
        Perguntas pergunta24 = new Perguntas("É um tipo de estrutura de dados em que a função de dispersão é a responsável por gerar um índice a partir de determinada chave; por causa das colisões, muitas tabelas de dispersão são aliadas com alguma outra estrutura de dados:",
                "matrizes",
                "vetores",
                "sort",
                "tabela hash",
                "");
        Perguntas pergunta25 = new Perguntas("Uma estrutura de dados onde cada nó mantém uma informação adicional, chamada fator de balanceamento, que indica a diferença de altura entre as subárvores esquerda e direita, é conhecida por árvore",
                "ordenada",
                "AVL",
                "binária",
                "de busca binária",
                "");
        Perguntas pergunta26 = new Perguntas("Quando as inserções e as remoções ocorrem sempre no mesmo lado da lista, trata-se de uma estrutura de dados denominada",
                "Pilha",
                "Fila",
                "Vetor",
                "Lista Circular",
                "");
        Perguntas pergunta27 = new Perguntas("Podem ser considerados como listas de informações armazenadas em posição contígua na memória.",
                "Nó",
                "Ponteiro",
                "Vetor",
                "Fita",
                "");
        Perguntas pergunta28 = new Perguntas("É uma estrutura de dados dividida em linhas e colunas. Desta forma, pode-se armazenar diversos valores dentro dela. Para obter um valor é necessário identificá-lo por meio do número da linha e da coluna onde está armazenado. Trata-se de",
                "Pilha",
                "Fila",
                "Matriz",
                "Vetor",
                "");
        Perguntas pergunta29 = new Perguntas("Assinale a alternativa INCORRETA",
                "O uso de vetores deve ser evitado em situações em que um conjunto de dados do mesmo tipo precisa ser armazenado em uma mesma estrutura.",
                "A estrutura de dados heap, que é eficiente para a implementação do método de ordenação heapsort, consiste em uma árvore binária completa e sua implementação mais simples ocorre na forma de array.",
                "Uma posição específica de um vetor pode ser acessada diretamente por meio de seu índice.",
                "Em uma árvore binária de busca, como em toda árvore binária, todos os nós têm grau máximo igual a Entretanto, nem toda árvore binária pode ser considerada uma árvore binária de busca.",
                "");
        Perguntas pergunta30 = new Perguntas("No contexto de estrutura de dados, uma pilha é",
                "Uma lista do tipo FIFO",
                "Um tipo de lista linear em que as operações de inserção e remoção são realizadas na extremidade denominada topo.",
                "um tipo de lista linear em que as operações de inserção e remoção são realizadas aleatoriamente.",
                "Um tipo de lista linear em que as operações de inserção são realizadas em uma extremidade e as operações de remoção são realizadas em outra extremidade.",
                "");
        Perguntas pergunta31 = new Perguntas("A estrutura de dados linear que obedece o seguinte critério: o último elemento inserido será o primeiro elemento a ser retirado (last in first out ? LIFO) é:",
                "Fila",
                "Árvore binária",
                "Árvore AVL",
                "Pilha",
                "");
        Perguntas pergunta32 = new Perguntas("A estrutura de dados composta por nós que apontam para o próximo elemento da lista, com exceção do último, que não aponta para ninguém, é denominada",
                "Pilha",
                "Árvore",
                "Fila",
                "Lista",
                "");
        Perguntas pergunta33 = new Perguntas("No que se refere às estrutura de dados, é incorreto afirmar",
                "LIFO refere-se à estrutura de dados do tipo pilha, que nada mais é do que uma lista linear, sem disciplina de acesso, onde o primeiro elemento a entrar é o último a sair.",
                "Guardar endereço de memória em nós normalmente identificados por previous ou next, é uma característica presente nas listas duplamente encadeadas.",
                "Um grafo com um único vértice e sem arestas é conhecido como dígrafo.",
                "Nos sistemas operacionais, a execução dos processos concorrentes ocorre segundo os princípios da estrutura FILO.",
                "");
        Perguntas pergunta34 = new Perguntas("O almoxarifado do Instituto Federal Goiano necessita de um sistema para o controle de entradas e saídas de materiais. Para cada saída de material, considera-se o custo do mais recente que tenha dado entrada no almoxarifado. O técnico deve desenvolver um algoritmo para tratar com uma estrutura de dados do tipo",
                "FIFO",
                "ARRAY",
                "LIFO",
                "LILO",
                "");
        Perguntas pergunta35 = new Perguntas("Se o nó raiz de uma árvore binária completa tiver nível 0 e essa árvore tiver profundidade 3, quantos nós folha ela terá?",
                "8",
                "16",
                "4",
                "1",
                "");
        Perguntas pergunta36 = new Perguntas("Uma estrutura de dados em lista duplamente encadeada permite na cadeia movimentos para",
                "frente apenas.",
                "cima e para baixo ou para frente e para trás.",
                "cima e para baixo apenas.",
                "frente e para trás, apenas.",
                "");
        Perguntas pergunta37 = new Perguntas("Considere uma estrutura de dados do tipo vetor. Com respeito a tal estrutura, é correto que seus componentes são, caracteristicamente,",
                "homogêneos e acesso não indexado",
                "heterogêneos e com acesso LIFO",
                "homogêneos e de acesso aleatório por intermédio de índices",
                "heterogêneos e com acesso FIFO",
                "");
        Perguntas pergunta38 = new Perguntas("Respeitando as ordens de inserção e de retirada dos dados, uma estrutura de",
                "pilha é também denominada LIFO ou FILO.",
                "fila é também denominada FIFO ou FILO.",
                "fila é também denominada FIFO ou LIFO.",
                "pilha é também denominada FIFO ou FILO.",
                "");
        Perguntas pergunta39 = new Perguntas("Uma fila dupla que se trata de uma lista linear na qual os elementos podem ser inseridos ou removidos de qualquer extremo denomina-se",
                "Grafo",
                "Deque",
                "Hashing",
                "Lista Aberta",
                "");
        Perguntas pergunta40 = new Perguntas("Se for eliminado o nó raiz de uma árvore, o que dela restar forma",
                "uma árvore binária",
                "uma sub-árvore",
                "outra árvore",
                "uma floresta",
                "");
        Perguntas pergunta41 = new Perguntas("Em uma estrutura do tipo árvore os nós de profundidade 1 são filhos do nó de profundidade",
                "2",
                "2 ou 3",
                "0",
                "1",
                "");
        Perguntas pergunta42 = new Perguntas("Em uma árvore binária completa de profundidade 4, se forem eliminados todos os nós de profundidade 2, teríamos quantas árvores?",
                "10",
                "9",
                "1",
                "3",
                "");
        Perguntas pergunta43 = new Perguntas("Acerca de estrutura de dados do tipo árvore podemos afirmar que um nó de grau zero é",
                "O primeiro nó de uma árvore.",
                "Um nó que não possui pai.",
                "Um nó que não possui irmãos",
                "Um nó que não possui filho.",
                "");
        Perguntas pergunta44 = new Perguntas("Partindo do princípio que uma lista de chamada é elaborada em ordem alfabética decrescente e o professor resolve fazer a chamada do último para o primeiro. O cenário descrito é característico da estrutura de dados do tipo",
                "Fila",
                "Pilha",
                "Árvore",
                "NDA",
                "");
        Perguntas pergunta45 = new Perguntas("Acerca de estrutura de dados do tipo árvore é CORRETO afirmar que a profundidade de um nó é:",
                "a distância entre este até a folha.",
                "a distância deste nó até a raiz.",
                "a quantidade de nós que são nele ligados.",
                "a quantidade de raiz que este possui.",
                "");
        Perguntas pergunta46 = new Perguntas("Não pertence ao grupo",
                "Pilha",
                "Raiz",
                "Fila",
                "Árvore",
                "");
        Perguntas pergunta47 = new Perguntas("Não pertence ao grupo",
                "Raiz", "Nó", "Folha", "Nível","");
        Perguntas pergunta48 = new Perguntas("É uma estrutura de dados em que cada elemento tem um ou mais elementos associados esses elementos são chamados de nó.",
                "Pilha",
                "Fila",
                "Grafo",
                "Árvore",
                "");
        Perguntas pergunta49 = new Perguntas("Assinale a alternativa correta",
                "Matrizes são vetores bidimensionais onde os elementos são acessados através de índices.",
                "Filas são estruturas baseadas no princípio LIFO, onde as operações de inserção e remoção são realizadas em extremidades diferentes.",
                "Existem duas funções que se aplicam a todas as filas: PUSH, que insere um dado no início da fila, e POP, que remove o item no final da fila. ",
                "NDA",
                "");
        Perguntas pergunta50 = new Perguntas("I - Estabelecer  um trajeto para percorrer todas as capitais de um país. II - Utilizar um mapa que indique as rodovias existentes e estabelecer uma ordem possível para percorrer todas as cidades. III - Encontrar um modo de percorrer todas a cidades, determinar o caminho mais curto para ir de uma cidade para outra. Dado os passos acima. Assinale a alternativa correspondente.",
                "Pilha",
                "Grafo",
                "Árvore",
                "NDA",
                "");
        Perguntas pergunta51 = new Perguntas("Estrutura de dados que caracteriza uma relação de hierarquia entre os elementos",
                "Grafo",
                "Matriz",
                "Árvore",
                "NDA",
                "");
        Perguntas pergunta52 = new Perguntas("Acerca de grafos assinale a alternativa onde não é correspondente",
                "Encontrar um caminho para ir de um objeto a outro seguindo as conexões",
                "Encontrar a menor distância entre um objeto e outro",
                "Encontrar outros objetos que podem ser alcançados a partir de um determinado objeto",
                "Representar o organograma de uma empresa inclusive organização hierárquica.",
                "");


        List<Perguntas> perguntas = new ArrayList<>();
                perguntas.add(pergunta1);
                        perguntas.add(pergunta2);
                                perguntas.add(pergunta3);
                                        perguntas.add(pergunta4);
                                                perguntas.add(pergunta5);
                                                        perguntas.add(pergunta6);
                                                                perguntas.add(pergunta7);
                                                                        perguntas.add(pergunta8);
                                                                                perguntas.add(pergunta9);
                                                                                        perguntas.add(pergunta10);
                                                                                                perguntas.add(pergunta11);
                                                                                                        perguntas.add(pergunta12);
                                                                                                                perguntas.add(pergunta13);
                                                                                                                        perguntas.add(pergunta14);
                                                                                                                                perguntas.add(pergunta15);
                                                                                                                                        perguntas.add(pergunta16);
                                                                                                                                                perguntas.add(pergunta17);
                                                                                                                                                        perguntas.add(pergunta18);
                                                                                                                                                                perguntas.add(pergunta19);
                                                                                                                                                                        perguntas.add(pergunta20);
                                                                                                                                                                                perguntas.add(pergunta21);
                                                                                                                                                                                        perguntas.add(pergunta22);
                                                                                                                                                                                                perguntas.add(pergunta23);
                                                                                                                                                                                                        perguntas.add(pergunta24);
                                                                                                                                                                                                                perguntas.add(pergunta25);
                                                                                                                                                                                                                        perguntas.add(pergunta26);
                                                                                                                                                                                                                                perguntas.add(pergunta27);
                                                                                                                                                                                                                                        perguntas.add(pergunta28);
                                                                                                                                                                                                                                                perguntas.add(pergunta29);
                                                                                                                                                                                                                                                        perguntas.add(pergunta30);
                                                                                                                                                                                                                                                                perguntas.add(pergunta31);
                                                                                                                                                                                                                                                                        perguntas.add(pergunta32);
                                                                                                                                                                                                                                                                                perguntas.add(pergunta33);
                                                                                                                                                                                                                                                                                        perguntas.add(pergunta34);
                                                                                                                                                                                                                                                                                                perguntas.add(pergunta35);
                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta36);
                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta37);
                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta38);
                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta39);
                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta40);
                                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta41);
                                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta42);
                                                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta43);
                                                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta44);
                                                                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta45);
                                                                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta46);
                                                                                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta47);
                                                                                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta48);
                                                                                                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta49);
                                                                                                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta50);
                                                                                                                                                                                                                                                                                                                                                                                                                                perguntas.add(pergunta51);
                                                                                                                                                                                                                                                                                                                                                                                                                                        perguntas.add(pergunta52);
        return perguntas;

    }


}
