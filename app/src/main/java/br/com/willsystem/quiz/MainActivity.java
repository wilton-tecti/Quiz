package br.com.willsystem.quiz;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.view.View.OnClickListener;


import java.util.List;

import br.com.willsystem.quiz.banco.DBUtil;
import br.com.willsystem.quiz.model.Perguntas;
import br.com.willsystem.quiz.repository.RepositoryPergunta;
import br.com.willsystem.quiz.util.Radon;

public class MainActivity extends AppCompatActivity implements OnClickListener {


    private Integer quantidadeDePerguntas = 15;
    private DBUtil db= null;
    private Perguntas pergunta;
    private String resposta = "";
    private Integer acertos = 0;
    private Integer contador = 0;
    private TextView txtCodigoPergunta, txtPerguntaBd, resultado, acerto, score;
    private RadioButton res1, res2, res3, res4;
    private Button btn;
    private RepositoryPergunta perguntaDB = new RepositoryPergunta(this);
    /*Lista com números aleatórios não repitidos, para posterior buscar cada pergunta questão por id*/
    private  List<Integer> valores = Radon.nextIntEspecific(quantidadeDePerguntas);
    private long retorno = 10;
    private String nome;
    private RadioGroup grupo;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("Instanciou MainActivity");

        /* Vincula os objetos da visão (XML) aos objetos java da class view*/

        txtCodigoPergunta = (TextView) findViewById(R.id.textCodPergunta);
        txtPerguntaBd = (TextView) findViewById(R.id.textPerguntaBd);
        score = (TextView) findViewById(R.id.totaldepontos);

        res1 = (RadioButton) findViewById(R.id.radioButton);
        res2 = (RadioButton) findViewById(R.id.radioButton2);
        res3 = (RadioButton) findViewById(R.id.radioButton3);
        res4 = (RadioButton) findViewById(R.id.radioButton4);
        btn =  (Button)   findViewById(R.id.btnResponder);
        grupo = (RadioGroup) findViewById(R.id.radioGroup);
        acerto = (TextView) findViewById(R.id.textAcerto);

        /*Executa os métodos, de clique nos botões*/

        btn.setOnClickListener(this);
        res1.setOnClickListener(this);
        res2.setOnClickListener(this);
        res3.setOnClickListener(this);
        res4.setOnClickListener(this);
        responde(contador);


       Intent intent = getIntent();
        nome = String.valueOf(intent.getStringExtra("nome"));
        btn.setText(nome);

    }

    public void responde(Integer cont){
        /* O método perguntaDB.find(), busca no banco de dados uma questão selva*/
        /* */
        pergunta = perguntaDB.find(valores.get(contador));
        res1.setText(pergunta.toString());
        res2.setText(pergunta.getQuestao()+"");

        txtCodigoPergunta.setText(String.valueOf(contador+1));
        txtPerguntaBd.setText(pergunta.getQuestao());
        res1.setText(pergunta.getResposta1());
        res2.setText(pergunta.getResposta2());
        res3.setText(pergunta.getResposta3());
        res4.setText(pergunta.getResposta4());
      //  btn.setText(param.getStringExtra("Nome"));


    }

    @Override
    public void onClick(View view) {
        if (view.equals(btn)){

            //perguntaDB.inserir();


        }else if(view.equals(res1)){
            resposta = "1";

        }else if(view.equals(res2)){
            resposta = "2";

        }else if(view.equals(res3)){
            resposta = "3";

        }else if(view.equals(res4)){
            resposta = "4";

        }
        res1.setEnabled(false);
        res2.setEnabled(false);
        res3.setEnabled(false);
        res4.setEnabled(false);
        verificaResposta();
      //  btn.setText("OK: "+ pergunta.getRespostacerta()+acertos+ "Res:"+resposta);
    }
    private void startTimer(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                    perguntaRespondida();
            }
        }, 1000);

    }

    public void verificaResposta() {
        if (resposta.equals(pergunta.getRespostacerta())) {
            acertos++;
            acerto.setText("Acertou");
            acerto.setTextColor(Color.GREEN);
            Log.d("STATUS", "Acertos: " + acertos);
        } else {
            acerto.setText("Errou");
            acerto.setTextColor(Color.RED);
        }
        contador++;

        startTimer();
        score.setText(String.valueOf(acertos * 1000));
        score.setTextColor(Color.BLUE);


    }
    public void perguntaRespondida(){
        acerto.setText("");
        res1.setEnabled(true);
        res2.setEnabled(true);
        res3.setEnabled(true);
        res4.setEnabled(true);
        if (contador < quantidadeDePerguntas){

            responde(contador);
            res1.clearFocus();
            res2.clearFocus();
            res3.clearFocus();
            res4.clearFocus();
            acerto.clearFocus();
        }else{
            Intent intent = new Intent(this,ResultActivity.class);
            intent.putExtra("pontos",String.valueOf(acertos * 1000));
            intent.putExtra("nome",nome);
            startActivity(intent);
            finish();
           //btn.setText("Você acertou "+acertos+" questões !!!");

        }

    }


}
