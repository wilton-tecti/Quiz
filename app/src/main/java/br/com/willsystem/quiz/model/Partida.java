package br.com.willsystem.quiz.model;

/**
 * Created by will on 20/09/16.
 */
public class Partida {

    private Integer idp;
    private String _id;
    private String pontos;
    private String jogador;
    private Integer __v;



    public Partida() {
    }

    public Partida( String jogador, String pontos) {
        this.idp = idp;
        this.jogador = jogador;
        this.pontos = pontos;

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Integer getId() {
        return idp;
    }

    public void setId(Integer id) {
        this.idp = id;
    }

    public String getPontos() {
        return pontos;
    }

    public void setPontos(String pontos) {
        this.pontos = pontos;
    }

    public String getJogador() {
        return jogador;
    }

    public void setJogador(String jogador) {
        this.jogador = jogador;
    }

    public Integer get__v() {
        return __v;
    }

    public void set__v(Integer __v) {
        this.__v = __v;
    }

    public String toString(){
        return this.jogador.toUpperCase()+" Total de Pontos: "+this.pontos;

    }
}
