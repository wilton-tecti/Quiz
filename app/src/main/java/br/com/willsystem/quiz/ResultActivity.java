package br.com.willsystem.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

import org.w3c.dom.Text;

import br.com.willsystem.quiz.api.RequestJson;
import br.com.willsystem.quiz.model.Partida;
import br.com.willsystem.quiz.repository.RepositoryPartida;

public class ResultActivity extends AppCompatActivity implements OnClickListener {

    private TextView pontos;
    private Button btninicio;
    private TextView nomejogador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        pontos = (TextView) findViewById(R.id.textPontos);
        btninicio = (Button) findViewById(R.id.buttonresinicio);
        nomejogador = (TextView) findViewById(R.id.textnomejogador);
        Intent intent = getIntent();
        String pontosjogador = intent.getStringExtra("pontos").toString();
        String nome = intent.getStringExtra("nome").toString();
        pontos.setText(pontosjogador);
        nomejogador.setText(nome);
        RepositoryPartida persistir = new RepositoryPartida(this);
        Partida partida = new Partida(nome,pontosjogador);
        persistir.salvar(partida);
        RequestJson conexaoApi = new RequestJson();
        conexaoApi.salvarPartidaRankOnline(partida);
        btninicio.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(btninicio)){
            Intent intent = new Intent(this,PrincipalActivity.class);
            intent.putExtra("nome",nomejogador.getText().toString());
            startActivity(intent);
            finish();
        }

    }
}
