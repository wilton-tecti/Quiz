package br.com.willsystem.quiz.model;

/**
 * Created by will on 11/08/16.
 */
public class Perguntas {

    private Integer id;
    private String questao;
    private String resposta1;
    private String resposta2;
    private String resposta3;
    private String resposta4;
    private String respostacerta;

    public Perguntas(){}
    public Perguntas(String questao, String resposta1, String resposta2, String resposta3, String resposta4, String respostacerta){
        this.questao = questao;
        this.resposta1 = resposta1;
        this.resposta2 = resposta2;
        this.resposta3 = resposta3;
        this.resposta4 = resposta4;
        this.respostacerta = respostacerta;


    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestao() {
        return questao;
    }

    public void setQuestao(String questao) {
        this.questao = questao;
    }

    public String getResposta1() {
        return resposta1;
    }

    public void setResposta1(String resposta1) {
        this.resposta1 = resposta1;
    }

    public String getResposta2() {
        return resposta2;
    }

    public void setResposta2(String resposta2) {
        this.resposta2 = resposta2;
    }

    public String getResposta3() {
        return resposta3;
    }

    public void setResposta3(String resposta3) {
        this.resposta3 = resposta3;
    }

    public String getResposta4() {
        return resposta4;
    }

    public void setResposta4(String resposta4) {
        this.resposta4 = resposta4;
    }

    public String getRespostacerta() {
        return respostacerta;
    }

    public void setRespostacerta(String respostacerta) {
        this.respostacerta = respostacerta;
    }

    public String toString(){return this.questao; }
}
