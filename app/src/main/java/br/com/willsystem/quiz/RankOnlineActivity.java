package br.com.willsystem.quiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.view.View.OnClickListener;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.quiz.api.VolleyApplication;
import br.com.willsystem.quiz.model.Partida;

public class RankOnlineActivity extends AppCompatActivity implements OnClickListener {

    private List<Partida> partidas = new ArrayList<>();
    private ArrayAdapter<Partida> adapterList;
    private ListView listView;
    private Button btnRankOnlineParaInicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank_online);
        listView =  (ListView)findViewById(R.id.listViewRankOnline);
        btnRankOnlineParaInicio = (Button) findViewById(R.id.btnRankOnlineParaInicio);
        btnRankOnlineParaInicio.setOnClickListener(this);
        requisicaoJson();

    }
    private void requisicaoJson(){
        String  url ="http://52.67.128.201/partidas";
        String url2 = "http://52.67.128.201/api/partidas";
        Log.e("url", url);
        try {
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url2, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            convertJsonParaPartida(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("json", error.getMessage());
                }
            });
            VolleyApplication.getInstance().getRequestQueue().add(request);

        }catch (Exception e ){
            Log.e("Exce",e.getMessage());
        }




    }

    private void convertJsonParaPartida(JSONArray response){

        try {
            Log.e("json", response.getString(0));
            Gson gson = new Gson();
            Type type = new TypeToken<Partida>(){}.getType();
            for (int i =0 ;i < response.length(); i++) {
                Partida partida = gson.fromJson(response.getString(i), Partida.class);
                this.partidas.add(partida);

            }
            Log.e("List",this.partidas.size()+"");
            adapterList = new ArrayAdapter<Partida>
                    (this,android.R.layout.simple_expandable_list_item_1,partidas);
            listView.setAdapter(adapterList);
        } catch (JsonSyntaxException j){
            Log.e("jsonSyntaxExce",j.getMessage());
            j.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rank_online, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnRankOnlineParaInicio)){
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
        }

    }
}
