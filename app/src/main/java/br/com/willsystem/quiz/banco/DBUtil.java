package br.com.willsystem.quiz.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import br.com.willsystem.quiz.repository.RepositoryPergunta;

public class DBUtil extends SQLiteOpenHelper {



    private String scriptCreate = "create table pergunta (_id integer primary "+
            "key autoincrement, questao text not null, "+
            "res1 text not null, res2 text not null," +
            "res3 text not null, res4 text not null," +
            "rescerta text not null);";
    private String scriptDelete =  "DROP TABLE IF EXISTS pergunta";
    private String createPartida = "CREATE TABLE partida (_id integer primary "+
                                    " key autoincrement," +
                                    "jogador text," +
                                    " pontos text);";
    private Context myctx;

    public DBUtil(Context ctx, String nomeBd, int versaoBanco) {

        super(ctx, nomeBd, null, versaoBanco);

        this.myctx = ctx;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(scriptCreate);
        db.execSQL(createPartida);
        Log.d("onCreate","Executado");

    }

    public void onUpgrade(SQLiteDatabase db,
                          int oldVersion, int newVersion) {
        Log.d("onUpdate","Executado");

        db.execSQL(scriptDelete);
        onCreate(db);
    }
}
